package pl.sda.currency_converter;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) throws IOException {
    showValueOfPLN(100,"usd", "eur", "chf","gbp");
    }

    public static URL jsonTodayURL(String currencyCode) throws MalformedURLException {
        String url = "http://api.nbp.pl/api/exchangerates/rates/c/" + currencyCode + "/?format=json";
        return new URL(url);
    }

    public static URL jsonMonthAgoURL(String currencyCode) throws MalformedURLException {
//        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateMinusMonth = LocalDate.now().minusDays(29);
        String url = "http://api.nbp.pl/api/exchangerates/rates/c/" + currencyCode + "/" + dateMinusMonth.toString() + "/?format=json";
        return new URL(url);
    }

    public static void showValueOfPLN(int amountOfPLN, String...currency ) throws IOException {

        List<URL> currencyURLs=new ArrayList<>();
        List<URL> currencyURLsMonthAgo=new ArrayList<>();

        for (int i = 0; i <currency.length ; i++) {
            currencyURLs.add(jsonTodayURL(currency[i]));
            currencyURLsMonthAgo.add(jsonMonthAgoURL(currency[i]));
        }

        System.out.println(amountOfPLN+" PLN is worth:");
        for (int i = 0; i < currencyURLs.size(); i++) {
            BufferedReader br = new BufferedReader(new InputStreamReader(currencyURLs.get(i).openStream()));
            CurrencyConverter currencyConverter = new Gson().fromJson(br.readLine(), CurrencyConverter.class);
            br = new BufferedReader(new InputStreamReader(currencyURLsMonthAgo.get(i).openStream()));
            CurrencyConverter currencyConverter2 = new Gson().fromJson(br.readLine(), CurrencyConverter.class);

            System.out.printf("%.2f" + " " + currencyConverter.getCode() + "%n",
                    (amountOfPLN / currencyConverter.rates.get(0).getAsk()));
            System.out.print("Gain/lost (comparing to last month) : ");
            System.out.printf("%.2f PLN %n", ((currencyConverter.rates.get(0).getBid()
                    * (amountOfPLN / currencyConverter2.rates.get(0).getAsk())) - amountOfPLN));

            br.close();
        }
    }
}
